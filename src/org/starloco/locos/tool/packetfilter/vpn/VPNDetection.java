/* Decompiler 12ms, total 151ms, lines 94 */
package vpn.detection;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public final class VPNDetection {
   private String api_key;
   private String api_url = "http://api.vpnblocker.net/v2/json/";
   private int api_timeout = 5000;

   public VPNDetection() {
      this.api_key = null;
   }

   public VPNDetection(String key) {
      this.api_key = key;
   }

   public VPNDetection(String key, int timeout) {
      this.api_key = key;
      this.api_timeout = timeout;
   }

   public void set_api_key(String key) {
      this.api_key = key;
   }

   public void set_api_timeout(int timeout) {
      this.api_timeout = timeout;
   }

   public void useSSL() {
      this.api_url = this.api_url.replace("http://", "https://");
   }

   public Response getResponse(String ip) throws IOException {
      String query_url = this.get_query_url(ip);
      String query_result = this.query(query_url, this.api_timeout, "Java-VPNDetection Library");
      return (Response)(new Gson()).fromJson(query_result, Response.class);
   }

   public String get_query_url(String ip) {
      String query_url;
      if (this.api_key == null) {
         query_url = this.api_url + ip;
      } else {
         query_url = this.api_url + ip + "/" + this.api_key;
      }

      return query_url;
   }

   public String query(String url, int timeout, String userAgent) throws MalformedURLException, IOException {
      StringBuilder response = new StringBuilder();
      URL website = new URL(url);
      URLConnection connection = website.openConnection();
      connection.setConnectTimeout(timeout);
      connection.setRequestProperty("User-Agent", userAgent);
      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
      Throwable var8 = null;

      try {
         while((url = in.readLine()) != null) {
            response.append(url);
         }

         in.close();
      } catch (Throwable var17) {
         var8 = var17;
         throw var17;
      } finally {
         if (in != null) {
            if (var8 != null) {
               try {
                  in.close();
               } catch (Throwable var16) {
                  var8.addSuppressed(var16);
               }
            } else {
               in.close();
            }
         }

      }

      return response.toString();
   }
}
