package vpn.detection;

import com.google.gson.annotations.SerializedName;

public class Response {
   public String status;
   public String msg;
   @SerializedName("package")
   public String getPackage;
   public String remaining_requests;
   public String ipaddress;
   @SerializedName("host-ip")
   public boolean hostip;
   public String hostname;
   public String org;
   public Response.CS country;
   public Response.CS subdivision;
   public String city;
   public String postal;
   public Response.latlon location;

   public class latlon {
      public double lat;
      @SerializedName("long")
      public double lon;
   }

   public class CS {
      public String name;
      public String code;
   }
}

